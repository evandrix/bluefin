start browser

open '/page-elements.html'

// present
assertElementPresent linkText:'Simple link'
assertElementPresent id:'id1'
assertElementPresent className:'class1'
assertElementPresent css:'#id1'
assertElementPresent css:'.class1'
assertElementPresent href:'http://google.com'

assertElementPresent  id:'submit1'
assertElementPresent  className:'class2'
assertElementPresent  css:'#submit1'
assertElementPresent  css:'.class2'
assertElementPresent  value:'Submit3'

assertElementPresent id:'div1'
assertElementPresent className:'div2'
assertElementPresent css:'#message.alert.alert-success'
assertElementPresent css:'div#message'

// missing
assertElementNotPresent  linkText:'Simple link_'
assertElementNotPresent  id:'id1_'
assertElementNotPresent  className:'class1_'
assertElementNotPresent  css:'#id1_'
assertElementNotPresent  css:'.class1_'
assertElementNotPresent  href:'http://outlook.com'

assertElementNotPresent  id:'submit1___'
assertElementNotPresent  className:'class2___'
assertElementNotPresent  css:'#submit1___'
assertElementNotPresent  css:'.class2___'
assertElementNotPresent  value:'Submit3___'

close browser