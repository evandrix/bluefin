// add some verbosity, handy for bluefin dev
debug on

// start default browser, which is Firefox for now
start browser

// open an URL
open 'http://demo.bluefinqa.org'
assertTitle 'Bluefin Demo HTML'

// you can open a relative path too, as long you have baseUrl set in your bluefin.proprties file
open '/'
assertTitle 'Bluefin Demo HTML'

// custom asserts
assertElementPresent href:'/page-text.html'
assertElementPresent linkText:'Check for page text'

// groovy comes in handy
['/page-text.html', '/page-elements.html', '/click.html', '/ajax.html', '/html.html'].each {
	assertElementPresent href:it
}

// take a guess ?!
close browser