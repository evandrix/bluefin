start browser

open '/tags/tryhtml_form_submit.htm'

assertElementPresent action:'demo_form.asp'
assertElementNotPresent action:'/some/cool/action.asp'

clear name:'FirstName'
type 'Luke', name:'FirstName'

clear name:'LastName'
type 'Skywalker', name:'LastName'

click value:'Submit'

assertTextPresent 'FirstName=Luke&LastName=Skywalker'

open '/tags/tryhtml_label.htm'

click value:'Submit'

assertTextPresent 'Nothing'

open '/tags/tryhtml_label.htm'

click id:'male'
click value:'Submit'

assertTextPresent 'sex=male'

close browser
