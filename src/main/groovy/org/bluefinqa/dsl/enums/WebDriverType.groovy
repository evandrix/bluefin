package org.bluefinqa.dsl.enums

enum WebDriverType {
	firefox('Firefox'), ff('Firefox'), chrome('Chrome'), ie('Internet Explorer')

	String name

	WebDriverType(String name) {
		this.name = name
	}
}
