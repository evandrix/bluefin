package org.bluefinqa.utils

class BluefinLogger {
	void log(def msg, def level) {
		println "[${level}] ${msg}"
	}

	void debug(def msg) {
		log msg, 'debug'
	}

	void info(def msg) {
		log msg, 'info'
	}

	void warn(def msg) {
		log msg, 'warn'
	}
}
