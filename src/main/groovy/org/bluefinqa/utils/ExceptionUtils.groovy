package org.bluefinqa.utils

import org.apache.commons.lang3.StringUtils

class ExceptionUtils {
	static int getLineNumber(Throwable e, String testname) {
		if (e != null) {
			StackTraceElement[] st = e.getStackTrace()

			for (StackTraceElement ste : st) {
				if (StringUtils.equals(testname, ste.getFileName())) {
					return ste.getLineNumber()
				}
			}
		}

		return -1
	}
}
