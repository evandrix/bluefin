package org.bluefinqa.test.unit

import static org.junit.Assert.*

import org.bluefinqa.utils.SelectorUtils
import org.junit.Test

class SelectorUtilsTest {
	@Test
	void test_get_by_from_selector_map() {
		assertNull SelectorUtils.getByFromSelectorMap(null)
		assertNull SelectorUtils.getByFromSelectorMap([])
		assertNull SelectorUtils.getByFromSelectorMap('aaa')
		assertNull SelectorUtils.getByFromSelectorMap(1234)
		assertNull SelectorUtils.getByFromSelectorMap([:])

		assertEquals 'aaa', SelectorUtils.getByFromSelectorMap([id:'aaa']).id
		assertEquals 'bbb', SelectorUtils.getByFromSelectorMap([className:'bbb']).className
		assertEquals '.ccc', SelectorUtils.getByFromSelectorMap([css:'.ccc']).selector
		assertEquals 'link1', SelectorUtils.getByFromSelectorMap([linkText:'link1']).linkText
		assertEquals '[href="http://aaa.com"]', SelectorUtils.getByFromSelectorMap([href:'http://aaa.com']).selector
		assertEquals '[action="/aaa/submit"]', SelectorUtils.getByFromSelectorMap([action:'/aaa/submit']).selector
		assertEquals '[name="name1"]', SelectorUtils.getByFromSelectorMap([name:'name1']).selector
		assertEquals '[value="Submit"]', SelectorUtils.getByFromSelectorMap([value:'Submit']).selector
		assertEquals '//aa/aa/aa', SelectorUtils.getByFromSelectorMap([xpath:'//aa/aa/aa']).xpathExpression
		assertEquals '//button[text()="Sign In"]', SelectorUtils.getByFromSelectorMap([buttonText:'Sign In']).xpathExpression
	}
}
